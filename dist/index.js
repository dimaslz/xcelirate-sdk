var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { HttpClient } from "./http-client";
var baseURL = "http://localhost:8082";
var Api = /** @class */ (function (_super) {
    __extends(Api, _super);
    function Api() {
        var _this = _super.call(this, baseURL) || this;
        _this.track = function (data) {
            return _this.instance.post("/track", { data: data });
        };
        return _this;
    }
    Api.getInstance = function () {
        if (!this.classInstance) {
            this.classInstance = new Api();
        }
        return this.classInstance;
    };
    Api._handleResponse = function (data) { return data.json(); };
    Api._handleError = function (error) { return Promise.reject(error); };
    return Api;
}(HttpClient));
var instance = Api.getInstance();
export default instance;
