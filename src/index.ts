import { HttpClient } from "./http-client";

const baseURL = "http://localhost:8082";
class Api extends HttpClient {
  private static classInstance: Api;

  private constructor() {
    super(baseURL);
  }

  public static getInstance() {
    if (!this.classInstance) {
      this.classInstance = new Api();
    }

    return this.classInstance;
  }

  static _handleResponse = (data: any) => data.json();

  static _handleError = (error: any) => Promise.reject(error);

  public track = (data) => {
    return this.instance.post("/track", { data });
  };
}

const instance = Api.getInstance();
export default instance;
