export abstract class HttpClient {
  protected instance: any;

  public constructor(baseURL: string) {
    this.instance = HttpClient.create(baseURL, {
      headers: {
        "Content-Type": "application/json",
      },
    });
  }

  static create(url: string, config: any) {
    return {
      post: (path = "", { data }: any = {}) =>
        fetch(`${url}${path}`, {
          ...config,
          method: "POST",
          body: JSON.stringify(data),
        })
          .then(this._handleResponse)
          .catch(this._handleError),

      // ... other methods
    };
  }

  static _handleResponse = (data: any) => {
    return data;
  };

  static _handleError = (error: any) => {
    return Promise.reject(error);
  };
}
